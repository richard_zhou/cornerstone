package com.cs.micro.file;

import com.cs.base.oauth2.EnableOauth2RC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @author wangjiahao
 * @version 1.0
 * @className FileServerApplication
 * @since 2019-04-03 10:44
 */
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@EnableOauth2RC
public class FileServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileServerApplication.class, args);
    }
}
