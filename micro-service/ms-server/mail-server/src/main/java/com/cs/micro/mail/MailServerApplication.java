package com.cs.micro.mail;

import com.cs.base.oauth2.EnableOauth2RC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @author wangjiahao
 * @version 1.0
 * @className MailServerApplication
 * @since 2019-03-08
 */
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@EnableOauth2RC
public class MailServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailServerApplication.class, args);
    }

}
