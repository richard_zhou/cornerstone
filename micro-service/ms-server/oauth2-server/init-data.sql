-- ----------------------------
-- Table structure for oauth_client_details 将请求的路径存在数据表
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
                                            `client_id` varchar(48) NOT NULL,
                                            `resource_ids` varchar(256) DEFAULT NULL,
                                            `client_secret` varchar(256) DEFAULT NULL,
                                            `scope` varchar(256) DEFAULT NULL,
                                            `authorized_grant_types` varchar(256) DEFAULT NULL,
                                            `web_server_redirect_uri` varchar(256) DEFAULT NULL,
                                            `authorities` varchar(256) DEFAULT NULL,
                                            `access_token_validity` int(11) DEFAULT NULL,
                                            `refresh_token_validity` int(11) DEFAULT NULL,
                                            `additional_information` varchar(4096) DEFAULT NULL,
                                            `autoapprove` varchar(256) DEFAULT NULL,
                                            PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
# 插入oauth数据
INSERT INTO conerstone.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) VALUES ('client', 'user', '$2a$10$2tPXA8Hrwqxi.WRTJAR0m.m.BNheOLfVP5fDvn9okQjcXHMObnUBW', 'all', 'client_credentials', null, 'oauth2', null, null, null, null);

# 插入角色
INSERT INTO `sys_role`
VALUES ('1', '1', '2019-02-27 15:47:15', 0, NULL, NULL, 'admin', '管理员', 0);
# 插入用户
INSERT INTO `sys_user`
VALUES ('1', '1', '2019-02-27 15:18:01', 0, NULL, NULL, 1, '管理员',
        '$2a$10$VwKECHVLs5NSFdq0rb/mp./P6yLjUEVR16MKRDS5YYB.GVaCUfksK', 0, 'admin');
# 插入用户据角色关联信息
INSERT INTO `sys_user_roles`
VALUES ('1', '1');

