/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: FanoutReceiver
 * Author:   liyuan
 * Date:     2019-04-02 15:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.micro.demo.customer.mq.receiver;

import com.cs.micro.demo.customer.config.RabbitConfig;
import com.cs.micro.demo.customer.mq.template.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author liyuan
 * @create 2019-04-02
 * @since 1.0.0
 */

@Component
public class FanoutReceiver {

    private ObjectMapper mapper = new ObjectMapper();

    // queues是指要监听的队列的名字
    @RabbitListener(queues = RabbitConfig.FANOUT_QUEUE1)
    public void receiveTopic1(String user) {
        System.out.println("【receiveFanout1监听到消息】" + user);
        User user1 = new User();
        try {
            user1 = mapper.readValue(user, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @RabbitListener(queues = RabbitConfig.FANOUT_QUEUE2)
    public void receiveTopic2(String user) {
        System.out.println("【receiveFanout2监听到消息】" + user);
        User user1 = new User();
        ;
        try {
            user1 = mapper.readValue(user, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}