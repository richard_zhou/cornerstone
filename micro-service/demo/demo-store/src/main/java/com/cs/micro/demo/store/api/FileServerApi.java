package com.cs.micro.demo.store.api;

import com.cs.base.cloud.feign.FeignConfig;
import com.cs.base.common.support.FileResult;
import com.cs.base.common.support.SimpleResult;
import com.cs.micro.demo.store.fallback.FileServerApiFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wangjiahao
 * @version 1.0
 * @className FileServerApi
 * @since 2019-04-03 11:33
 */
@Component
@FeignClient(value = "file-server",configuration = FeignConfig.class,fallback = FileServerApiFallback.class)
public interface FileServerApi {

    @PostMapping(value="/i/uploadFile",produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    SimpleResult<FileResult> uploadImage(@RequestPart(value="file") MultipartFile file,
                                               @RequestParam(value = "title") String title);

}
