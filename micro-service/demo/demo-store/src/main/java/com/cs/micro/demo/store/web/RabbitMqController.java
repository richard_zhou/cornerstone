/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: RabbitMqController
 * Author:   liyuan
 * Date:     2019-04-02 15:53
 * Description: mq测试接口
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.micro.demo.store.web;

import com.cs.base.common.support.SimpleResult;
import com.cs.micro.demo.store.mq.sender.FanoutSender;
import com.cs.micro.demo.store.mq.template.User;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 〈一句话功能简述〉<br>
 * 〈mq测试接口〉
 *
 * @author liyuan
 * @create 2019-04-02
 * @since 1.0.0
 */
@Component
@Slf4j
@RestController
@Api(tags = "mq测试接口")
public class RabbitMqController {


    @Autowired
    private FanoutSender fanoutSender;

    @PostMapping("w/mq/send")
    public SimpleResult<User> sendMsg(@RequestBody User user) {
        fanoutSender.send(user);

        return SimpleResult.ok(user);
    }

}