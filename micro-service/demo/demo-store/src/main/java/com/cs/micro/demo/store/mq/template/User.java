/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: User
 * Author:   liyuan
 * Date:     2019-04-02 15:37
 * Description: 消息发送模板类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.micro.demo.store.mq.template;

/**
 * 〈一句话功能简述〉<br>
 * 〈消息发送模板类〉
 *
 * @author liyuan
 * @create 2019-04-02
 * @since 1.0.0
 */


import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {

    private String id;

    private String name;

}