/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: FanoutSender
 * Author:   liyuan
 * Date:     2019-04-02 15:37
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.micro.demo.store.mq.sender;

import com.cs.micro.demo.store.config.RabbitConfig;
import com.cs.micro.demo.store.mq.template.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 〈Fanout模式〉<br>
 * 〈〉
 *
 * @author liyuan
 * @create 2019-04-02
 * @since 1.0.0
 */
@Component
@Slf4j
public class FanoutSender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send(User user) {
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            log.error("json序列化失败==>", e);
        }
        this.rabbitTemplate.convertAndSend(RabbitConfig.FANOUT_EXCHANGE, "", json);
        System.out.println("【FanoutSender发送消息】" + json);
    }
}