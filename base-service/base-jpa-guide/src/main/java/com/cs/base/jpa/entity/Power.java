package com.cs.base.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * @author wangjiahao
 * @version 1.0
 * @className Power
 * @since 2019-03-29 16:41
 */
@Setter
@Getter
@Entity(name = "t_power")
public class Power {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String code;

    @ManyToMany(mappedBy = "powers")
    @JsonIgnore
    private Set<Role> roles;

}
