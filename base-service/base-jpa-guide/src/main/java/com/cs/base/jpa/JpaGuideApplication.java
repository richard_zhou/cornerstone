package com.cs.base.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangjiahao
 * @version 1.0
 * @className JpaGuideApplication
 * @since 2019-03-29 14:47
 */
@SpringBootApplication
public class JpaGuideApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaGuideApplication.class, args);
    }

}
