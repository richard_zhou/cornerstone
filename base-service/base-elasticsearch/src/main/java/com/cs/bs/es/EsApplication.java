/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: Application
 * Author:   liyuan
 * Date:     2019-03-22 16:07
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.bs.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author liyuan
 * @create 2019-03-22
 * @since 1.0.0
 */
@SpringBootApplication
public class EsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsApplication.class, args);
    }

}